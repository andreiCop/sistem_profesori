package com.usm.login.repository;

import com.usm.login.model.UserResult;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("userResultRepository")
public interface UserResultRepository extends JpaRepository<UserResult, Long> {
    List<UserResult> findAll();
}
