package com.usm.login.repository;

import com.usm.login.model.Questions;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface QuestionsRepository extends JpaRepository<Questions, Integer> {
    List<Questions> findAll();
}
