package com.usm.login.dto;

import com.usm.login.model.QuestionsList;

import java.util.ArrayList;
import java.util.List;

public class QuestionsListDto {
    public List<QuestionsList> questionsList = new ArrayList<>();

    public void addQuestions(QuestionsList questionsList) {
        this.questionsList.add(questionsList);
    }

    public List<QuestionsList> getQuestionsList() {
        return questionsList;
    }

    public void setQuestionsListList(List<QuestionsList> questionsList) {
        this.questionsList = questionsList;
    }
}
