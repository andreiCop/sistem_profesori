package com.usm.login.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class QuestionsList {

    private int id;
    private String questions;
    private Set<Role> roles;
    private String options1;
    private String options2;
    private String options3;
    private String options4;
    private String options5;
    private List<QuestionsList> questionsList = new ArrayList<>();

    public QuestionsList() {
    }

    public QuestionsList(int id, String questions, Set<Role> roles, String options1, String options2, String options3, String options4, String options5) {
        this.id = id;
        this.questions = questions;
        this.roles = roles;
        this.options1 = options1;
        this.options2 = options2;
        this.options3 = options3;
        this.options4 = options4;
        this.options5 = options5;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getQuestions() {
        return questions;
    }

    public void setQuestions(String questions) {
        this.questions = questions;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public String getOptions1() {
        return options1;
    }

    public void setOptions1(String options1) {
        this.options1 = options1;
    }

    public String getOptions2() {
        return options2;
    }

    public void setOptions2(String options2) {
        this.options2 = options2;
    }

    public String getOptions3() {
        return options3;
    }

    public void setOptions3(String options3) {
        this.options3 = options3;
    }

    public String getOptions4() {
        return options4;
    }

    public void setOptions4(String options4) {
        this.options4 = options4;
    }

    public String getOptions5() {
        return options5;
    }

    public void setOptions5(String options5) {
        this.options5 = options5;
    }

    public List<QuestionsList> getQuestionsList() {
        return questionsList;
    }

    public void setQuestionsList(List<QuestionsList> questionsList) {
        this.questionsList = questionsList;
    }
}
