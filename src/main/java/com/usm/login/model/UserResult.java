package com.usm.login.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "userResult")
public class UserResult {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_result_id")
    private int id;
    @Column(name = "email_user")
    private String email;
    @Column(name = "questions_user")
    private String questionsResult;
    @Column(name = "point_user")
    private String pointResult;
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "user_result_data", joinColumns = @JoinColumn(name = "user_result_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles;
}
