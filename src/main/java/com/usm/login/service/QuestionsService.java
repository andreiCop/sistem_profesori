package com.usm.login.service;

import com.usm.login.model.Questions;
import com.usm.login.model.Role;
import com.usm.login.repository.QuestionsRepository;
import com.usm.login.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

@Service("questionsService")
@Component
public class QuestionsService {

    private QuestionsRepository questionsRepository;
    private RoleRepository roleRepository;

    @Autowired
    public QuestionsService(
            @Qualifier("roleRepository") RoleRepository roleRepository,
            @Qualifier("questionsRepository") QuestionsRepository questionsRepository) {
        this.roleRepository = roleRepository;
        this.questionsRepository = questionsRepository;
    }

    public Questions saveQuestions(Questions questions) {
        Role userRole = roleRepository.findByRole("STUDENT");
        questions.setRoles(new HashSet<Role>(Arrays.asList(userRole)));
        return questionsRepository.save(questions);
    }

    public List<Questions> findAllQuestions() {
        return questionsRepository.findAll();
    }
}
