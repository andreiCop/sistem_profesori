package com.usm.login.service;

import com.usm.login.model.Role;
import com.usm.login.model.UserResult;
import com.usm.login.repository.RoleRepository;
import com.usm.login.repository.UserRepository;
import com.usm.login.repository.UserResultRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashSet;

@Service("userResultService")
@Component
@Configurable
public class UserResultService {

    private UserRepository userRepository;
    private RoleRepository roleRepository;
    private UserResultRepository userResultRepository;

    public UserResultService() {
    }

    @Autowired
    public UserResultService(@Qualifier("roleRepository") RoleRepository roleRepository,
                             @Qualifier("userRepository") UserRepository userRepository,
                             @Qualifier("userResultRepository") UserResultRepository userResultRepository) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.userResultRepository = userResultRepository;
    }

    public UserResult saveUserResult(UserResult userResult) {


        Role userRole = roleRepository.findByRole("STUDENT");
        userResult.setRoles(new HashSet<Role>(Arrays.asList(userRole)));
        return userResultRepository.save(userResult);
    }
}
