package com.usm.login.controller;

import com.usm.login.model.Questions;
import com.usm.login.model.User;
import com.usm.login.service.QuestionsService;
import com.usm.login.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
@PreAuthorize("hasRole('ADMIN')")
public class AdminController {

    @Qualifier("userService")
    @Autowired
    private UserService userService;

    @Qualifier("questionsService")
    @Autowired
    private QuestionsService questionsService;

    @RequestMapping(value = "/admin/home", method = RequestMethod.GET)
    public ModelAndView homeAdmin() {
        ModelAndView modelAndView = new ModelAndView();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByEmail(auth.getName());
        modelAndView.addObject("userName", "Welcome " + user.getName() + " " + user.getLastName() + " (" + user.getEmail() + ")");
        modelAndView.addObject("adminMessage", "Content Available Only for Users with Admin Role");
        modelAndView.setViewName("admin/home");
        return modelAndView;
    }

    @RequestMapping(value = "/addQuestions", method = RequestMethod.GET)
    public ModelAndView addQuestions() {
        ModelAndView modelAndView = new ModelAndView();
        /*Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByEmail(auth.getName());
        modelAndView.addObject("userName", "Welcome " + user.getName() + " " + user.getLastName() + " (" + user.getEmail() + ")");
        modelAndView.addObject("adminMessage","Content Available Only for Users with Admin Role");*/
        modelAndView.setViewName("admin/addQuestions");
        return modelAndView;
    }

    @RequestMapping(value = "/addQuestions", method = RequestMethod.POST)
    public ModelAndView saveQuestions(@ModelAttribute("saveAddQuestions") @Valid Questions questions) {
        ModelAndView modelAndView = new ModelAndView();

        questionsService.saveQuestions(questions);
        // modelAndView.setViewName("redirect:" + "/admin/addQuestions");
        modelAndView.setViewName("admin/addQuestions");
        return modelAndView;
    }
}
