package com.usm.login.controller;

import com.usm.login.dto.QuestionsListDto;
import com.usm.login.model.Questions;
import com.usm.login.model.QuestionsList;
import com.usm.login.model.User;
import com.usm.login.model.UserResult;
import com.usm.login.service.QuestionsService;
import com.usm.login.service.UserResultService;
import com.usm.login.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
@PreAuthorize("hasRole('STUDENT')")
@Component
@Configurable
public class StudentController {

    @Qualifier("userService")
    @Autowired
    private UserService userService;


    @Qualifier("questionsService")
    @Autowired
    private QuestionsService questionsService;


    @Qualifier("userResultService")
    @Autowired
    private UserResultService userResultService;

    @Secured("STUDENT")
    @PreAuthorize("hasRole('STUDENT')")
    @RequestMapping(value = "/student/home2", method = RequestMethod.GET)
    public ModelAndView homestudent() {
        ModelAndView modelAndView = new ModelAndView();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByEmail(auth.getName());
        modelAndView.addObject("userName", "Welcome " + user.getName() + " " + user.getLastName() + " (" + user.getEmail() + ")");
        modelAndView.addObject("Student mesajeMessage", "Content Available Only for Users with Student Role");
        modelAndView.setViewName("student/home2");
        return modelAndView;
    }

    @RequestMapping(value = {"/formular"}, method = RequestMethod.GET)
    public ModelAndView formular() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("formular1");
        return modelAndView;
    }

    @RequestMapping(value = {"/form1"}, method = RequestMethod.GET)
    public ModelAndView formular2(Model model) {
        QuestionsListDto questionsListDto = new QuestionsListDto();
        List<QuestionsList> questionsLists = new ArrayList<>();
        List<Questions> questions = questionsService.findAllQuestions();

        for (Questions questions1 : questions) {
            questionsLists.add(new QuestionsList(questions1.getId(), questions1.getQuestions(), questions1.getRoles(), "a", "a", "a", "a", "a"));
        }

        ModelAndView modelAndView = new ModelAndView();

        //modelAndView.addObject("questionsLists", questionsLists);
        modelAndView.addObject("questionsLists", questionsLists);
        model.addAttribute("form", questionsListDto);
        modelAndView.setViewName("student/form1");
        return modelAndView;
    }

    @RequestMapping(value = "/form1", method = RequestMethod.POST)
    public ModelAndView saveQuestions(@ModelAttribute("form") QuestionsList questionsLists3, @ModelAttribute("form") @Valid ArrayList<QuestionsList> questionsLists
    ) {
        ModelAndView modelAndView = new ModelAndView();
        List<QuestionsList> questionsLists1 = questionsLists;

        List<QuestionsList> questionsLists2 = questionsLists1.get(0).getQuestionsList();

        // modelAndView.setViewName("redirect:" + "/admin/addQuestions"); @ModelAttribute(value = "saveForm1")HashMap<String, String> questionsLists2


        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByEmail(auth.getName());

        List<Questions> questions = questionsService.findAllQuestions();

        int i = 0;


        for (QuestionsList questionsList : questionsLists2) {
            UserResult userResult = new UserResult();
            String question1 = questionsList.getOptions1();
            String question2 = questionsList.getOptions2();
            String question3 = questionsList.getOptions3();
            String question4 = questionsList.getOptions4();
            String question5 = questionsList.getOptions5();
            String resultOptions = new String();
            String question = questions.get(i).getQuestions();
            i++;


            if (!"null".equals(question1))
                resultOptions = question1;
            else if (!"null".equals(question2))
                resultOptions = question2;
            else if (!"null".equals(question3))
                resultOptions = question3;
            else if (!"null".equals(question4))
                resultOptions = question4;
            else if (!"null".equals(question5))
                resultOptions = question5;
            else resultOptions = "1";

            userResult.setEmail(user.getEmail());
            userResult.setQuestionsResult(question);
            userResult.setPointResult(resultOptions);
            try {
                userResultService.saveUserResult(userResult);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        modelAndView.setViewName("student/form1");
        return modelAndView;
    }

}
